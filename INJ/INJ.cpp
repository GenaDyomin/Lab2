#include "stdafx.h"
#include <math.h>
#include <iostream>
using namespace std;
void menu()
{
	cout << "1: e^x" << endl;
	cout << "2: sinx" << endl;
	cout << "3: cosx" << endl;
	cout << "4: ln(1+x)" << endl;
	cout << "5: (1+x)^a" << endl;
	cout << "6: arctgx" << endl;
	cout << "7: sin(x)^3" << endl;
	cout << "8: cos(x)^2" << endl;
	cout << "9: chx" << endl;
	cout << "10: shx" << endl;
	cout << "11: 1/(1+x)" << endl;
	cout << "12: 1/(1-x)" << endl;
	cout << "13: change an accuracy" << endl;
	cout << "14: exit" << endl;
}
double e_const_result(double x, long double exp)
{
	int fact = 1;
	double  el = 0.0;
	double sqr = 1.0;
	double sum = 0;
	for (int n = 0;; n++)
	{
		if (n == 0)
			fact = 1;
		else
			fact = fact*n;
		sqr = sqr*x;
		el = (sqr) / fact;
		if (el<exp)
		{
			return sum + el;
		}
		else
			sum = sum + el;
	}
}
void e_const(long double exp)
{
	double x;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "Enter x: " << endl;
		cin.clear();
		while (cin.get() != '\n');

	}
	double sum;
	sum = e_const_result(x, exp);
	cout << sum << endl;
	system("pause");
}
float sin_result(float x, long double exp)
{
	double el = 0.0;
	float sum = 0;
	int degree;
	double sqr;
	int fact = 1;
	int rate;
	int step;
	for (int n = 1;; n++)
	{
		degree = (2 * n) - 1;
		sqr = 1.0;
		rate = -1;
		step = n + 1;
		fact = 1;
		for (int i = degree; i>0; i--)
		{
			sqr = sqr*x;
			fact = i*fact;
		}
		if (step % 2 == 0)
		{
			rate = 1;
		}
		else
			rate = -1;
		el = (rate*sqr) / fact;
		if (el < exp)
		{
			if (sum == 0.0)
			{

				return el;
			}
			else
			{
				return sum + el;
			}
		}
		else
			sum = sum + el;
	}
}
void sinus(long double exp)
{
	double x;
	cout << "Enter x: " << endl;
	cin >> x;
	while (cin.fail() == 1)
	{
		cout << "Enter again !\n";
		cin.clear();
		cin >> x;
	}
	float sum;
	sum = sin_result(x, exp);
	cout << sum << endl;
	system("pause");
}
double calculate_alpha(long double exp, double x, int a)
{
	int fact;
	double el = 0.0;
	double sum = 0.0;
	int up = 1;
	double sqr = 1.0;
	for (int n = 0;; n++)
	{
		sqr = sqr*x;
		for (int q = 0; q >= n + 1; q++)
		{
			up = (a - q)*up;
		}
		if (n == 0)
			fact = 1;
		else
			fact = fact*n;
		el = ((up) / fact)*sqr;
		if (el < exp)
		{
			if (sum == 0.0)
			{

				return el;
			}
			else

				return sum + el;
		}
		else
			sum = sum + el;
	}
}
void alpha(long double exp)
{
	int a;
	double x;
	double sum;
	cout << "enter |x|<1: " << endl;
	while (!(cin >> x) || (x >= 1) || (x <= -1))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter |x|<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');

	}
	cout << "enter ALPHA: " << endl;
	while (!(cin >> a))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter a: " << endl;
		cin.clear();
		while (cin.get() != '\n');

	}
	sum = calculate_alpha(exp, x, a);
	cout << sum;
	system("pause");
	system("cls");
}
float calculate_arctg(long double exp, double x)

{
	double el = 0.0;
	int degree;
	double sqr = 1.0;
	int rate;

	for (int n = 0;; n++)
	{
		float sum = 0.0;
		for (int n = 0;; n++)
		{
			degree = (2 * n) + 1;
			rate = -1;
			for (int i = 0; i < degree; i++)
			{
				sqr = sqr*x;
			};
			if (n % 2 == 0)
			{
				rate = -1;
			}
			else
				rate = 1;
			el = (rate*sqr) / degree;
			if (el < exp)
			{
				if (sum == 0.0)
				{

					return el;
				}
				else
					return sum + el;
			}
			else
				sum = sum + el;

		}
	}
}
void arctgx(long double exp)
{
	double sum;
	double x;
	cout << "|x|<=1" << endl;
	cout << "Enter x: " << endl;
	while (!(cin >> x) || (x>1) || (x<-1))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter |x|<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');

	}
	sum = calculate_arctg(exp, x);
	cout << sum;
	system("pause");
}
double cos_result(double x, long double exp)
{
	/*double el = 0.0;
	int degree;
	double sqr;
	double sum = 0;
	int rate;
	for (int n = 0;; n++)
	{
	degree = 2 * n;
	sqr = 1.0;
	rate = -1;
	for (int i = degree; i > 0; i--)
	{
	sqr = sqr*x;
	}
	for (int i = n; i > 0; i--)
	{
	rate *= rate;
	}
	el = (rate*sqr) / fact(degree);
	if (el < exp)
	{
	return sum+el;
	}
	else
	sum = sum + el;
	}*/
	double s = 1, n = 0, a = 1;
	int i;
	for (i = 1; i <= 10; i++)
	{
		a = -(a*x*x) / ((2 * n + 2));
		s = s + a;
		n++;
		return s;
	}

}
void cosinus(long double exp)
{
	double x;
	double sum = 0;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter |x|<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');

	}
	sum = cos_result(x, exp);
	cout << sum;
	system("pause");
}
double log_result(double x, long double exp)
{
	double el = 0.0;
	double sum = 0;
	double 	sqr = 1.0;
	int fact = 1;
	double rate = -1;
	for (int n = 1;; n++)
	{
		sqr = sqr*x;
		fact = fact*n;
		el = (rate*sqr) / fact;
		if (el < exp)
		{
			return sum + el;
		}
		else
			sum = sum + el;
	}

}
void logarifm(long double exp)
{
	double x;
	double sum = 0;
	cout << "-1<x<=1" << endl;
	cout << "enter x: " << endl;
	while (!(cin >> x) || (x < -1 || x>1))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "Enter x: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	sum = log_result(x, exp);
	cout << sum;
	system("pause");
}
float sinus_result(float x, long double exp)
{
	double el = 0.0;
	float sum = 0;
	int fact = 1;
	int degree;
	float sqr;//
	float two;//
	int rate;
	int step;
	int f;
	for (int n = 1;; n++)
	{
		double el = 0.0;

		float sum = 0;

		int degree;
		float sqr;//
		float two;//
		int rate;
		int step;
		int f;
		int fact = 1;
		for (int n = 1;; n++)
		{

			degree = (2 * n) - 1;
			f = 2 * n;
			sqr = 1.0;
			two = 1.0;
			rate = -1.0;
			step = n + 1;
			for (int i = f; i>0; i--)
			{
				sqr = sqr*x;
			}
			for (int i = step; i>0; i--)
			{
				rate *= rate;
			}
			for (int i = degree; i>0; i--)
			{
				two = two * 2;
			}
			fact = fact*n;
			el = (two*rate*sqr) / fact;
			//sum = sum + el;
			if (el<exp)
			{

				return (1 - sum);
			}
			else
				sum = sum + el;
		}


	}
}
void sin_menu(long double exp)
{
	float x;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "Enter x: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	float sum;
	sum = sinus_result(x, exp);
	cout << sum << endl;
	system("pause");
}
float cosinus_result(float x, long double exp)
{
	double el = 0.0;
	float sum = 0;
	int degree;
	float sqr;
	float two;
	int rate = -1;
	int fact = 1;
	int step;
	int f;
	for (int n = 1;; n++)
	{
		degree = (2 * n) - 1;
		f = 2 * n;
		sqr = 1.0;
		two = 1.0;
		rate = -1.0;
		step = n + 1;
		for (int i = f; i>0; i--)
		{
			sqr = sqr*x;
		}
		if (step % 2 == 0)
		{
			rate = -1;
		}
		else
			rate = 1;
		for (int i = degree; i>0; i--)
		{
			two = two * 2;
		}
		for (int i = degree; i>0; i--)
		{
			fact = fact*degree;
		}
		el = (two*rate*sqr) / fact;

		if (el<exp)
		{
			return (1 - sum + el);
		}
		else
			sum = sum + el;
	}

}
void cos_menu(long double exp)
{
	float x;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "Enter x: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	float sum;
	sum = cosinus_result(x, exp);
	cout << sum << endl;
	system("pause");
}
float calculate_shx(long double exp, double x)
{
	double el = 0.0;
	int degree;
	int fact;
	double sqr = 1.0;
	int rate;
	float sum = 0.0;
	for (int n = 0;; n++)
	{

		degree = (2 * n) + 1;
		fact = 1;
		for (int i = 0; i < degree; i++)
		{
			sqr = sqr*x;
		};
		for (int i = degree; i > 0; i--)
		{
			fact = i*fact;
		}
		el = sqr / fact;
		if (el < exp)
		{
			if (sum == 0.0)
			{

				return el;
			}
			else
			{
				return sum + el;
			}
		}
		else
			sum = sum + el;
	}
}
void shx(double exp)
{
	double sum;
	double x;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter |x|<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	sum = calculate_shx(exp, x);
	cout << sum;
	system("pause");
}
float calculate_�hx(double exp, double x)
{
	double el = 0.0;
	int degree;
	int fact;
	double sqr = 1.0;
	int rate;
	float sum = 0.0;
	for (int n = 0;; n++)
	{

		degree = (2 * n);
		fact = 1;
		rate = -1;
		for (int i = 0; i < degree; i++)
		{
			sqr = sqr*x;
		};
		for (int i = degree; i > 0; i--)
		{
			fact = i*fact;
		}
		el = sqr / fact;
		if (el < exp)
		{
			if (sum == 0.0)
			{

				return el;
			}
			else
			{
				return sum + el;
			}
		}
		else
			sum = sum + el;

	}
}
void �hx(double exp)
{
	double sum;
	double x;
	cout << "Enter x: " << endl;
	while (!(cin >> x))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter |x|<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	sum = calculate_�hx(exp, x);
	cout << sum;
	system("pause");
}
double calculate_Xplus1(float x, double exp)
{
	double el = 0.0;
	double sqr = 1.0;
	int rate;
	double sum = 0.0;
	for (int n = 0;; n++)
	{
		sqr = sqr*x;
		if (n % 2 == 0)
		{
			rate = 1;
		}
		else
			rate = -1;
		el = rate*sqr;
		if (el < exp)
		{
			if (sum == 0.0)
			{

				return el;
			}
			else
			{
				return sum + el;
			}
		}
		else
			sum = sum + el;
	}
}
void Xplus1(double exp)
{
	double sum;
	float x;
	cout << "Enter -1<x<1: ";
	while (!(cin >> x) || (x >= 1 || x <= -1))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter -1<x<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	sum = calculate_Xplus1(x, exp);
	cout << sum;
	system("pause");
}
double calculate_Xminus1(float x, double exp)
{
	double el = 0.0;
	double sqr = 1.0;
	double sum = 0.0;
	for (int n = 0;; n++)
	{
		sqr = sqr*x;
		if (sqr< exp)
		{
			if (sum == 0.0)
			{

				return sqr;
			}
			else
			{
				return sum + sqr;
			}
		}
		else
			sum = sum + sqr;
	}
}
void Xminus1(double exp)
{
	double sum;
	float x;
	cout << "Enter -1<x<1: ";
	while (!(cin >> x) || (x >= 1 || x <= -1))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "enter -1<x<1: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	sum = calculate_Xminus1(x, exp);
	cout << sum;
	system("pause");
}
int main()
{
	int a;
	double exp;
	double x = 0;
	cout << "Enter an accuracy: " << endl;
	while (!(cin >> exp))
	{
		system("cls");
		cout << "Error!" << endl;
		cout << "Enter exp: " << endl;
		cin.clear();
		while (cin.get() != '\n');
	}
	int k = 1;
	int l;
	while (k != 0)
	{
		system("cls");
		cout << "Choise number of menu: " << endl;
		menu();
		while (!(cin >> l))
		{
			system("cls");
			cout << "Error!" << endl;
			menu();
			cout << "Choise number of menu: " << endl;
			cin.clear();
			while (cin.get() != '\n');
		}
		switch (l)
		{
			system("cls");
		case 1:
			system("cls");
			e_const(exp);
			break;
		case 2:
			system("cls");
			sinus(exp);
			break;
		case 3:
			system("cls");
			cosinus(exp);
			break;
		case 4:
			system("cls");
			logarifm(exp);
			break;
		case 5:
			system("cls");
			alpha(exp);
			break;
		case 6:
			system("cls");
			arctgx(exp);
			break;
		case 7:
			system("cls");
			sin_menu(exp);
			break;
		case 8:
			system("cls");
			cos_menu(exp);
			break;
		case 9:
			system("cls");
			�hx(exp);
			break;
		case 10:
			system("cls");
			shx(exp);
			break;
		case 11:
			system("cls");
			Xplus1(exp);
			break;
		case 12:
			system("cls");
			Xminus1(exp);
			break;
		case 13:
			system("cls");
			cout << "Old accuracy: " << exp << endl;
			cout << "Enter new accuracy: ";
			cin >> exp;
			break;
		case 14:
			k = 0;
			break;
		default:
			system("cls");
			while (l < 1 || l>9)
			{
				cout << "Selected item is not present, repeat input" << endl;
				menu();
				cin >> l;
				system("cls");
			};
			break;
		}
	}
	return 0;
}
