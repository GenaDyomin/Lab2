#include "stdafx.h"
#include "CppUnitTest.h"
#include "../INJ/INJ.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(Arctg_test1)
		{
			long double exp = 0.0000001;
			float x = 0.5;
			float result = -0.5;
			Assert::AreEqual(result, calculate_arctg(exp, x), (wchar_t*)L"good!");
		}
		TEST_METHOD(Arctg_test2)
		{
			long double exp = 0.000001;
			double x = -0.5;
			float result = 0.521226;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_arctg(exp, x), delta, (wchar_t*)L"good!");
		}
		TEST_METHOD(Arctg_test3)
		{
			long double exp = 0.000001;
			double x = -0.78;
			float result = 0.927674;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_arctg(exp, x), delta, (wchar_t*)L"good!");
		}

		TEST_METHOD(Alpha_test1)
		{
			long double exp = 0.000001;
			double x = 0.1;
			int a = 5;
			double result = 0.11;
			Assert::AreEqual(result, calculate_alpha(exp, x, a), 0.01, (wchar_t*)L"good!");
		}
		TEST_METHOD(Alpha_test2)
		{
			long double exp = 0.000001;
			double x = -0.36;
			int a = 65;
			double result = -0.36;
			Assert::AreEqual(result, calculate_alpha(exp, x, a), 0.01, (wchar_t*)L"good!");
		}
		TEST_METHOD(Alpha_test3)
		{
			long double exp = 0.000001;
			double x = 0.35;
			int a = 5;
			Assert::AreEqual(0.4725, calculate_alpha(exp, x, a), 0.0001, (wchar_t*)L"good!");
		}

		TEST_METHOD(e_const_test1)
		{
			double exp = 0.001;
			double x = 0.1;
			double result = 0.1109;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp), 0.1/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(e_const_test2)
		{
			double exp = 0.001;
			double x = 0.2;
			double result = 0.24;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp), 0.01/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(e_const_test3)
		{
			double exp = 0.001;
			double x = -1;
			double result = -1;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp), 0.01/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(sinus_test1)
		{
			double exp = 0.000001;
			double x = -0.1;
			float result = -0.1;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sinus_test2)
		{
			double exp = 0.000001;
			double x = 0.5;
			float result = 0.479167;
			float delta = 0.000001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp), delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sinus_test3)
		{
			double exp = 0.000001;
			double x = 0.6978;
			float result = 0.641171;
			float delta = 0.000001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp), delta/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(Cos_test1)
		{
			double exp = 0.000001;
			double x = 0.1;
			double result = 0.995;
			Assert::AreEqual(result, cos_result(x, exp), 0.001/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(Cos_test2)
		{
			double exp = 0.000001;
			double x = -1;
			double result = 0.5;
			Assert::AreEqual(result, cos_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(Cos_test3)
		{
			double exp = 0.000001;
			double x = 0.5;
			double result = 0.875;
			Assert::AreEqual(result, cos_result(x, exp), 0.001/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(log_test1)
		{
			double exp = 0.000001;
			double x = -0.5;
			double result = 0.375;
			Assert::AreEqual(result, log_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(log_test2)
		{
			double exp = 0.000001;
			double x = 0.5;
			double result = -0.5;
			Assert::AreEqual(result, log_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(log_test3)
		{
			double exp = 0.000001;
			double x = -0.3654;
			double result = 0.298641;
			Assert::AreEqual(result, log_result(x, exp), 0.000001/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(sin_test_result1)
		{
			long double exp = 0.000001;
			double x = -0.3648;
			float sum = 0.648564;
			float delta = 0.000001;
			Assert::AreEqual(sum, sinus_result(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(sin_test_result2)
		{
			long double exp = 0.000001;
			double x = 0.6;
			float sum = -0.610347;
			float delta = 0.000001;
			Assert::AreEqual(sum, sinus_result(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(sin_test_result3)
		{
			long double exp = 0.00001;
			double x = 0.3125;
			float sum = 0.761051;
			float delta = 0.000001;
			Assert::AreEqual(sum, sinus_result(x, exp), delta, (wchar_t*)L"True!");
		}

		TEST_METHOD(cos_test_result1)
		{
			long double exp = 0.000001;
			double x = 0.0156399999;
			float sum = 0.999511;
			float delta = 0.000001;
			Assert::AreEqual(sum, cosinus_result(x, exp), delta, (wchar_t*)L"True!");
		}

		TEST_METHOD(cos_test_result2)
		{
			long double exp = 0.000001;
			double x = 0.486200005;
			float sum = 0.527219;
			float delta = 0.000001;
			Assert::AreEqual(sum, cosinus_result(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(cos_test_result3)
		{
			long double exp = 0.000001;
			double x = -0.5;
			float sum = 0.5;
			Assert::AreEqual(sum, cosinus_result(x, exp), (wchar_t*)L"True!");
		}

		TEST_METHOD(chx_test1)
		{
			long double exp = 0.000001;
			float x = 0.132;
			float result = 1.00871;
			float delta = 0.00001;
			Assert::AreEqual(result, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}

		TEST_METHOD(chx_test2)
		{
			long double exp = 0.000001;
			float x = -0.32;
			float result = 1.05124;
			float delta = 0.00001;
			Assert::AreEqual(result, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(chx_test3)
		{
			long double exp = 0.000001;
			float x = 1;
			float result = 1.54308;
			float delta = 0.00001;
			Assert::AreEqual(result, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}

		TEST_METHOD(shx_test1)
		{
			long double exp = 0.000001;
			float x = 0.28;
			float result = 0.281025;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(shx_test2)
		{
			long double exp = 0.000001;
			float x = 0.69;
			float result = 0.728075;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(shx_test3)
		{
			long double exp = 0.000001;
			float x = -0.789;
			float result = -0.789;
			float delta = 0.001;
			Assert::AreEqual(result, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xplus1_test1)
		{
			long double exp = 0.00001;
			float x = 0.1;
			double result = 0.09;
			double delta = 0.01;
			Assert::AreEqual(result, calculate_Xplus1(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xplus1_test2)
		{
			long double exp = 0.00001;
			float x = 0.5;
			double result = 0.25;
			double delta = 0.01;
			Assert::AreEqual(result, calculate_Xplus1(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xplus1_test3)
		{
			long double exp = 0.00001;
			float x = -0.5;
			double result = -0.5;
			double delta = 0.1;
			Assert::AreEqual(result, calculate_Xplus1(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xminus_test1)
		{
			long double exp = 0.00001;
			float x = 0.6;
			double result = 1.49999;
			double delta = 0.00001;
			Assert::AreEqual(result, calculate_Xminus1(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xminus_test2)
		{
			long double exp = 0.00001;
			float x = 0.7;
			double result = 2.33332;
			double delta = 0.00001;
			Assert::AreEqual(result, calculate_Xminus1(x, exp), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(calculate_Xminus_test3)
		{
			long double exp = 0.00001;
			float x = 0.78;
			double result = 3.54542;
			double delta = 0.00001;
			Assert::AreEqual(result, calculate_Xminus1(x, exp), delta, (wchar_t*)L"True!");
		}

	};
}