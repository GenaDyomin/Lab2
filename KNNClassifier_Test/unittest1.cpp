#include "stdafx.h"
#include "CppUnitTest.h"
#include "../INJ/INJ.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(Arctg_test1)
		{
			long double exp = 0.000001;
			double x = 1.0;
			float result = -1.0;
			Assert::AreEqual(result, calculate_arctg(exp, x), (wchar_t*)L"good!");
		}
		TEST_METHOD(Arctg_test2)
		{
			long double exp = 0.000001;
			double x = -0.5;
			float result = 0.521226;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_arctg(exp, x),delta, (wchar_t*)L"good!");
		}
		TEST_METHOD(Arctg_test3)
		{
			long double exp = 0.000001;
			double x = -0.78;
			float result = 0.927674;
			float delta = 0.000001;
			Assert::AreEqual(result, calculate_arctg(exp, x),delta, (wchar_t*)L"good!");
		}

		TEST_METHOD(Alpha_test1)
		{
			long double exp = 0.000001;
			double d = 0.36;
			int a = 5;
			double result = 0.4896;
			Assert::AreEqual(result, calculate_alpha(exp, d, a), (wchar_t*)L"good!");
		}
		TEST_METHOD(Alpha_test2)
		{
			long double exp = 0.000001;
			double d = 0.1;
			int a = 5;
			double result = 0.11;
			Assert::AreEqual(result, calculate_alpha(exp, d, a),0.001, (wchar_t*)L"good!");
		}
		TEST_METHOD(Alpha_test3)
		{
			long double exp = 0.000001;
			double d = -0.36;
			int a = 6;
			double result = -0.36;
			Assert::AreEqual(result, calculate_alpha(exp, d, a), (wchar_t*)L"good!");
		}

		TEST_METHOD(e_const_test1)
		{
			double exp = 0.001;
			double x = 0.1;
			double result = 0.1105;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp),0.00001/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(e_const_test2)
		{
			double exp = 0.001;
			double x = 0.2;
			double result = 0.244267;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp),0.000001/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(e_const_test3)
		{
			double exp = 0.001;
			double x = -1;
			double result = -1;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, e_const_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
	
		TEST_METHOD(sin_test1)
		{
			double exp = 0.001;
			double x = 0.1;
			float result = -0.1;
			float delta;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sin_test2)
		{
			double exp = 0.001;
			double x = -0.5;
			float result = 0.4375;
			float delta=0.00001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sin_test3)
		{
			double exp = 0.001;
			double x = -0.3125;
			float result = 0.297241;
			float delta=0.000001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sin_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		
		TEST_METHOD(sinus_test1)
		{
			double exp = 0.000001;
			double x = -0.6;
			float result = 0.048;
			float delta = 0.001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sinus_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sinus_test2)
		{
			double exp = 0.000001;
			double x = -0.333;
			float result = 0.00820579;
			float delta = 0.00000001;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sinus_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(sinus_test3)
		{
			double exp = 0.000001;
			double x = 4;
			float result = 0;
			// TODO: ���������� ����� ��� ������ �����
			Assert::AreEqual(result, sinus_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
	
		TEST_METHOD(log_test1)
		{
			double exp = 0.000001;
			double x = 0.5;
			double result = -0.5;	
			Assert::AreEqual(result, log_result(x, exp)/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(log_test2)
		{
			double exp = 0.000001;
			double x = -0.5;
			double result = 0.375;
			double delta =0.001 ;
			Assert::AreEqual(result, log_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(log_test3)
		{
			double exp = 0.000001;
			double x = -0.3654;
			double result = 0.298641;
			double delta = 0.000001;
			Assert::AreEqual(result, log_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(cos_test1)
		{
			double exp = 0.000001;
			double x = 0.1;
			double result = 0.995;
			double delta = 0.001;
			Assert::AreEqual(result, cos_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(cos_test2)
		{
			double exp = 0.000001;
			double x = 0.5;
			double result = 0.875;
			double delta = 0.001;
			Assert::AreEqual(result, cos_result(x, exp),delta/*, (wchar_t*)L"True"*/);
		}
		TEST_METHOD(cos_test3)
		{
			double exp = 0.000001;
			double x = -1;
			double result = 0.5;
			Assert::AreEqual(result, cos_result(x, exp)/*, (wchar_t*)L"True"*/);
		}

		TEST_METHOD(cos_test_result1)
		{
			long double exp = 0.000001;
			float x = 0.23;
			float sum = 0.8942;
			float delta = 0.001;
			Assert::AreEqual(sum, cosinus_result(x, exp), delta,(wchar_t*)L"True!");
		}
		TEST_METHOD(cos_test_result2)
		{
			long double exp = 0.000001;
			float x = 1;
			float sum = -1;
			Assert::AreEqual(sum, cosinus_result(x, exp), (wchar_t*)L"True!");
		}
		TEST_METHOD(cos_test_result3)
		{
			long double exp = 0.000001;
			float x = -0.36;
			float sum = 0.7408;
			float delta = 0.0001;
			Assert::AreEqual(sum, cosinus_result(x, exp),delta, (wchar_t*)L"True!");
		}
	
		TEST_METHOD(ch_test1)
		{
			long double exp = 0.000001;
			float x = 0.132;
			float sum = 1.00871;
			float delta = 0.00001;
			Assert::AreEqual(sum, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(ch_test2)
		{
			long double exp = 0.000001;
			float x = -0.32;
			float sum = 1.05124;
			float delta = 0.00001;
			Assert::AreEqual(sum, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(ch_test3)
		{
			long double exp = 0.000001;
			float x = 1;
			float sum = 1.54308;
			float delta = 0.00001;
			Assert::AreEqual(sum, calculate_�hx(exp, x), delta, (wchar_t*)L"True!");
		}
	
		TEST_METHOD(sh_test1)
		{
			long double exp = 0.000001;
			float x = 0.28;
			float sum = 0.281025;
			float delta = 0.000001;
			Assert::AreEqual(sum, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(sh_test2)
		{
			long double exp = 0.000001;
			float x = 0.69;
			float sum = 0.728075;
			float delta = 0.000001;
			Assert::AreEqual(sum, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
		TEST_METHOD(sh_test3)
		{
			long double exp = 0.000001;
			float x = -0.789;
			float sum = -0.789;
			float delta = 0.001;
			Assert::AreEqual(sum, calculate_shx(exp, x), delta, (wchar_t*)L"True!");
		}
	};
}